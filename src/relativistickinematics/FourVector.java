package relativistickinematics;

class FourVector
{
    // store the components of the FourVector
    double E;
    double px;
    double py;
    double pz;

    FourVector()
    {
        E = px = py = pz = 0.;
    }

    FourVector(double inE, double inpx, double inpy, double inpz)
    {
        E = inE;
        px = inpx;
        py = inpy;
        pz = inpz;
    }
    
    double momentum()
    {
        double p = px*px+py*py+pz*pz;
        p = Math.sqrt(p);
        return p;
    }

    double mass()
    {
        return Math.sqrt(E*E - momentum()*momentum());
    }

    double gamma()
    {
        return (E/mass());
    }

    double beta()
    {
        return (momentum()/E);
    }

    double radius(double B)
    {
        return (momentum()/(300.*B));
    }

}
