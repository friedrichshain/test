package relativistickinematics;

import java.util.Scanner;

class RelativisticKinematics {

    static Scanner keyboard = new Scanner(System.in);

    public static void main(String[] args) {
//        FourVector particle = new FourVector();
//        particle.E = 247.5;
//        particle.px = 100.;
//        particle.py = 0.0;
//        particle.pz = 200.;
        FourVector particle = new FourVector(247.5, 100., 0., 200.);

        System.out.println("YParticle with fourvector (E,px,py,pz) = ("
                + particle.E + ", "
                + particle.px + ", "
                + particle.py + ", "
                + particle.pz + ") MeV.");

        System.out.println("Mass = " + particle.mass() + " MeV");

        System.out.println("Momentum = " + particle.momentum() + " MeV");

        System.out.println("gamma = " + particle.gamma());

        double c = 299792458; // m/s
        double beta = particle.beta();
        double v = beta * c;
        System.out.println("beta = " + beta);
        System.out.println("v = " + v + " m/s");

        double decaylength = 2.19e-6 * particle.gamma() * v;
        System.out.println("average decay length = " + decaylength + " m");

        double B = 1.8; // T
        double R = particle.radius(B);
        System.out.println("radius of curvature in B = " + B
                + " T is R = " + R + " m");

        FourVector a = new FourVector();
        FourVector b = new FourVector();
        FourVector d = new FourVector();

        System.out.println("Four-vector calculations");

        System.out.println("Particle 1");
        System.out.println("Enter E, px, py, pz in MeV");
        a.E = keyboard.nextDouble();
        a.px = keyboard.nextDouble();
        a.py = keyboard.nextDouble();
        a.pz = keyboard.nextDouble();

        System.out.println("Particle 2");
        System.out.println("Enter E, px, py, pz in MeV");
        b.E = keyboard.nextDouble();
        b.px = keyboard.nextDouble();
        b.py = keyboard.nextDouble();
        b.pz = keyboard.nextDouble();

        System.out.println("Particle 1: p = " + a.momentum() + " MeV, m = " + a.mass() + " MeV");
        System.out.println("Particle 2: p = " + b.momentum() + " MeV, m = " + b.mass() + " MeV");

        d.E = a.E + b.E;
        d.px = a.px + b.px;
        d.py = a.py + b.py;
        d.pz = a.pz + b.pz;
        System.out.println("Particle 3: E = " + d.E + " MeV, px = " + d.px
                + " MeV, py = " + d.py
                + " MeV, pz = " + d.pz);
        System.out.println("Particle 3: p = " + d.momentum() + " MeV, m = " + d.mass() + " MeV");

    }
}
